import 'assets/css/style.css';
import 'assets/css/custom.css';
import { Route, Routes, Navigate } from 'react-router-dom';
import Navbar from 'components/Navbar';
import Footer from 'components/Footer';
import HomePage from 'page/HomePage';
import ShopPage from 'page/ShopPage';
import DetailProduct from 'page/DetailProduct';


import LoginPage from 'page/LoginPage';
import MyProfile from 'page/MyProfile';

const RequireAuth = ({ children }) => {
    const isLoggedIn = window.localStorage.getItem("isLoggedIn");
    return isLoggedIn ? children : <Navigate to="/login" />
};

const App = () => {
    return (
        <>
         <div className="container">
                   
                    <Navbar/>
                    <Routes>
                        <Route path='/' element={<HomePage />} />
                        <Route path='/shop' element={<ShopPage />}/>
                        <Route path='/shop/:id' element={<DetailProduct />} />
                      
                        <Route path='/profile' element={
                        <RequireAuth>
                            <MyProfile />
                        </RequireAuth>
                    } />
                        <Route path='/login' element={<LoginPage/>} />
                      
                        
                    </Routes>
                    <Footer/>
                </div>
                  </>
    );
}

export default App;
