import React, { useState } from 'react'
import axios from 'axios';
import Loading from 'components/Loading';

const LoginPage = () => {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [isLoading, setIsLoading] = useState(false);


    const handleSubmit = async e => {
        e.preventDefault();
        const data = {

            username: username,
            password: password
        }
        setIsLoading(true);
        axios.post(`https://api-tdp-2022.vercel.app/api/login`, data)
            .then(result => {

                window.localStorage.setItem('token', result.data.token);
                window.localStorage.setItem('username', username)
                window.localStorage.setItem("isLoggedIn", true);
                window.location.href = "/";

            })
            .catch(function (error) {
                alert(error?.response?.data?.message);
                setIsLoading(false);
            });

        console.log("username", username)
        console.log("password", password)
    }

    return (
        <>
            {
                isLoading ?<Loading /> :
                    <div className="content padding" style={{ maxWidth: '1564px' }}>
                        <div className="container padding-32" id="contact" >
                            <h3 className="border-bottom border-light-grey padding-16">Login</h3>
                            <p>Lets get in touch and talk about your next project.</p>
                            <form onSubmit={handleSubmit}>
                                <input className="input border" type="text" onChange={e => setUserName(e.target.value)} placeholder="Username" required name="username" />
                                <input className="input section border" onChange={e => setPassword(e.target.value)} type="password" placeholder="Password" required name="Password" />
                                <button className="button black section" type="submit" >
                                    <i className="fa fa-paper-plane" />
                                    Login
                                </button>
                            </form>
                        </div >
                    </div>
            }
        </>
    )
}

export default LoginPage

