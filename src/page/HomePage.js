import React, { useEffect, useState } from 'react'
import axios from 'axios';
import banner from 'assets/img/architect.jpg';
import map from 'assets/img/map.jpg';
import Home from 'components/Home';
import Loading from 'components/Loading';




const HomePage = () => {
   
    const [dataList, setdataList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    
    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/categories`)
            .then(function (response) {
                const { data } = response;
                let datas = data.data;
                setdataList(datas);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])
    console.log('dataList', dataList)
    return (
        <div>

            {
                isLoading ?
                <Loading />
                    
                    : <>
                        <header className="display-container content wide" style={{ maxWidth: '1500px' }} id="home">
                            <img className="image" src={banner} alt="Architecture" width={1500} height={800} />
                            <div className="display-middle center">
                                <h1 className="xxlarge text-white">
                                    <span className="padding black opacity-min">
                                        <b>EDTS</b>
                                    </span>
                                    <span className="hide-small text-light-grey">Mart</span>
                                </h1>
                            </div>
                        </header>
                        <div className="content padding" style={{ maxWidth: '1564px' }}>
                            <div className="container padding-32" id="projects">
                                <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
                            </div>
                            <div className="row-padding">
                                {dataList.map((item) => {
                                    return <Home key={item.id} item={item} />
                                }

                                )}
                                <div>
                                </div>
                            </div>
                        </div>

                        <div className="container padding-32">
                            <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
                        </div>

                    </>
            }
        </div>
    )

}



export default HomePage
