import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import ProdukInfo from 'components/ProdukInfo';
import Loading from 'components/Loading';

const DetailProduct = () => {
    const [item, setitem] = useState({})
    const [isLoading, setisLoading] = useState(false)
    let params = useParams();


    useEffect(() => {
        setisLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products/${params?.id}`)
            .then(function (response) {
                const { data } = response;
                setitem(data.data);
                setisLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("item", item)


    return (
        <div>

            {
                isLoading ?
                   <Loading />
                    : <>
                        <div className="content padding" style={{ maxWidth: '1564px' }}>
                            <div className="container padding-32" id="about">
                                <h3 className="border-bottom border-light-grey padding-16">Product Category</h3>
                            </div>
                            <ProdukInfo key={item.id} item={item} />
                        </div>
                    </>
            }
        </div >
    )
}

export default DetailProduct