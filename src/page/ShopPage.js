import 'assets/css/style.css';
import 'assets/css/custom.css';
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Loading from 'components/Loading';
import BannerAllProduct from 'components/BannerAllProduct';
import Shop from 'components/Shop';
import { useSearchParams } from 'react-router-dom';



const ShopPage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [allProduct, setAllProduct] = useState([]);
    const [byCategory, setbyCategory] = useState([]);
    const [param] = useSearchParams()
    const category = param.get('category')
    const [isproduk, setproduk] = useState(''); 

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products`)
            .then(function (response) {
                const { data } = response;
                setAllProduct(data.data.productList);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])
    console.log("All product", allProduct)

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products?category=${category}`)
            .then(function (response) {
                const { data } = response;
                setbyCategory(data.data.productList);
                setproduk(data.data.categoryDetail.name)
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])
    console.log("category", byCategory)

    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            {
                isLoading ? <Loading /> :
                    <> {category == null ?
                        <BannerAllProduct /> :
                        <div className="container padding-32" id="about">
                            <h3 className="border-bottom border-light-grey padding-16">
                                Product Category {isproduk}
                            </h3>
                        </div>}
                        
                        <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                        {category == null ? 
                        
                                allProduct?.map(item => {
                                    return <Shop key={item.id} item={item} />;
                                }) : byCategory?.map(item => {
                                  
                                    return <Shop key={item.id} item={item} />;
                                })}

                        </div>
                    </>
            }
        </div>
    )
}

export default ShopPage