import axios from 'axios';
import Loading from 'components/Loading';
import React, { useEffect, useState } from 'react'

const MyProfile = () => {
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);

    const [profile, setProfile] = useState({
        name: '',
        email: '',
        phoneNumber: ''
    })

    const [password, setPassword] = useState({
        password: '',
        passwordBaru: ''
    })

    const [notif, setNotif] = useState({
        showMessage: false,
        field: '',
        messageContent: ''
    })


    const username = window.localStorage.getItem("username");
    const getProfile = async () => {
        try {
            let respons = await axios.get(`https://api-tdp-2022.vercel.app/api/profile/${username}`);
            setProfile(respons.data.data);
            setisLoading(false);
        }
        catch (e) {
            setisError(true);
        }
    }

    useEffect(() => {
        setisLoading(true);
        getProfile();
    }, [])

    const handleChange = (e) => {
        let data = { ...profile };
        data[e.target.name] = e.target.value;
        setProfile(data)

        let pass = { ...password };
        pass[e.target.name] = e.target.value;
        setPassword(pass)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let data = { ...profile };

        if (profile.name === "") {
            setNotif({
                showMessage: true,
                field: 'name',
                messageContent: "Name is required"
            })
        } else if (profile.email === "") {
            setNotif({
                showMessage: true,
                field: 'email',
                messageContent: "Email is required"
            })
        } else if (profile.email === "") {
            setNotif({
                showMessage: true,
                field: 'phoneNumber',
                messageContent: "Phone Number is required"
            })
        } else if (password.password === "") {
            setNotif({
                showMessage: true,
                field: 'password',
                messageContent: "Password is required"
            })
        } else if (password.passwordBaru === "") {
            setNotif({
                showMessage: true,
                field: 'passwordBaru',
                messageContent: "RePassword is required"
            })
        } else if (password.password !== password.passwordBaru) {
            setNotif({
                showMessage: true,
                field: 'passwordBaru',
                messageContent: "Password is not same"
            })
        } else {
            const update = {
                name: profile.name,
                email: profile.email,
                phoneNumber: profile.phoneNumber,
                password: password.passwordBaru
            }

            axios.put(` https://api-tdp-2022.vercel.app/api/profile/${username} `, update)
                .then(result => {
                    if (result) {
                        // console.log(result?.data?.message)
                        alert(result?.data?.message)
                        window.location.replace("/");
                    }
                })
        }
    }

    return (
        <div>
            {
                isLoading ?
                    <Loading />
                    : <>
                        <div className="content padding" style={{ maxWidth: '1564px' }}>
                            <div className="container padding-32" id="contact" >
                                <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                                <p>Lets get in touch and talk about your next project.</p>
                                <form onSubmit={handleSubmit}>
                                    <input className="input section border" type="text" placeholder="Name" name="name" value={profile.name} onChange={handleChange} />
                                    {
                                        (notif.showMessage && (notif.field === "name")) ?
                                            <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                                            : ''
                                    }
                                    <input className="input section border" type="text" placeholder="Email" name="email" value={profile.email} onChange={handleChange} />
                                    {
                                        (notif.showMessage && (notif.field === "email")) ?
                                            <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                                            : ''
                                    }
                                    <input className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12} value={profile.phoneNumber} onChange={handleChange} />
                                    {
                                        (notif.showMessage && (notif.field === "phoneNumber")) ?
                                            <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                                            : ''
                                    }
                                    <input className="input section border" type="password" placeholder="Password" name="password" value={password.password} onChange={handleChange} />
                                    {
                                        (notif.showMessage && (notif.field === "password")) ?
                                            <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                                            : ''
                                    }
                                    <input className="input section border" type="password" placeholder="Retype Password" name="passwordBaru" value={password.passwordBaru} onChange={handleChange} />
                                    {
                                        (notif.showMessage && (notif.field === "passwordBaru")) ?
                                            <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                                            : ''
                                    }
                                    <button className="button black section" type="submit">
                                        <i className="fa fa-paper-plane" /> Update
                                    </button>
                                </form>
                            </div>
                        </div>
                    </>
            } </div>
    )
}



export default MyProfile