import React from 'react'
import { Link } from "react-router-dom";

const Navbar = () => {
    const isLoggedIn = window.localStorage.getItem("isLoggedIn");

    const handleLogout = () => {
        localStorage.clear()
        window.location.reload()
    }
    return (
        <div className="top" >
            <div className="bar white wide padding card">
                <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
                <div className="right hide-small">
                    <Link to="/" className="bar-item button">Home</Link>
                    <Link to="/shop" className="bar-item button">Shop</Link>
                    {/* <Link to="/profile" className="bar-item button">My Profile</Link> */}
                    {/* <Link to="/login" className="bar-item button">Login</Link> */}
                    {
                        isLoggedIn ?

                            <Link to="/profile" className="bar-item button">My Profile</Link>
                            : ''
                    }

                    {
                        isLoggedIn ?
                            <div className="right hide-small">

                                <div style={{ cursor: 'pointer' }} className="bar-item button" onClick={handleLogout}>Logout</div>

                            </div> :

                            <Link to="/login" className="bar-item button">Login</Link>

                    }
                </div>

            </div>
        </div>
    )
}

export default Navbar