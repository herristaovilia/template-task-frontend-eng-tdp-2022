import React from 'react'
import { useState } from 'react'


const ProdukInfo = ({ item }) => {
    const [quantityp, setQuantityp] = useState('')
    const isLoggedIn = window.localStorage.getItem("isLoggedIn");

    function myFunction() {
        alert("Product added to cart successfully");
    }
    function currencyFormat(num) {
        return 'Rp ' + num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    let num = item.price;
    let diskon = 1 - (item?.discount / 100);
    let priceAfterDiscount = currencyFormat(num * quantityp);
    let subTotal = currencyFormat(num * quantityp * diskon)
    let dataPrice = parseInt(item.price)
    let price = currencyFormat(dataPrice)

    return (

        <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
            <div className="col l3 m6 margin-bottom">
                <div className="product-tumb">
                    <img src={item.image} alt="Product 1" />
                </div>
            </div>
            <div className="col m6 margin-bottom">
                <h3>{item.title}</h3>
                <div style={{ marginBottom: '32px' }}>
                    <span>Category : <strong>{item.category}g</strong></span>
                    <span style={{ marginLeft: '30px' }}>Review : <strong>{item.rate}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Stock : <strong>{item.stock}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Discount : <strong>{item.discount} %</strong></span>
                </div>
                <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                    {price}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    {item.description}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    <div><strong>Quantity : </strong></div>
                    {item.stock > 0 ? <input type="number" min={0} max={item.stock} className="input section border" name="total" placeholder='0' onChange={event => setQuantityp(event.target.value)} /> : ""}
                </div>
                <div name="result" style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                    Sub Total : {subTotal}
                    {item.discount > 0 ? <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp {priceAfterDiscount}</strong></span> : ""}
                </div>


                {item.stock > 0 ? (quantityp == 0 ?
                    <button className='button light-grey block' disabled={true} onClick={myFunction} >Add to cart</button> :
                    <button className='button light-grey block' onClick={myFunction} >Add to cart</button>) :
                    <button className='button light-grey block' disabled={true}>Empty Stock</button>}





            </div>
        </div>
    )
}

export default ProdukInfo