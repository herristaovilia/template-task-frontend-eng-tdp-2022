import React from 'react'

const Loading = () => {
    return (
        <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center', marginTop:"100px" }}>
            Loading . . .
        </div>
    )
}

export default Loading